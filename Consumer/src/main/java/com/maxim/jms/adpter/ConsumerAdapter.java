package com.maxim.jms.adpter;

import java.net.UnknownHostException;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

@Component
public class ConsumerAdapter {

	private static Logger logger= LogManager.getLogger(ConsumerAdapter.class.getName());

	public void sendToMongo(String json) throws UnknownHostException	 {
		logger.info("Getting in sendToMongo");
		MongoCredential credential=MongoCredential.createCredential("sdc", "sdc_dev", "sdc".toCharArray());
		MongoClient client = new MongoClient(new ServerAddress(
				"ds127391.mlab.com", 27391),
				Arrays.asList(credential));

		MongoDatabase db=client.getDatabase("sdc_dev");
		
		MongoCollection<Document> contact=db.getCollection("contact");
		
		Document document=Document.parse(json);
		contact.insertOne(document);
		logger.info("Sent to MongoDB");
		client.close();
	}

}
