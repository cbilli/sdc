package com.maxim.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.maxim.jms.adpter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {

	private static Logger logger= LogManager.getLogger(ConsumerListener.class.getName());
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	ConsumerAdapter consumerAdapter;
	
	public void onMessage(Message message) {
		logger.info("Inside onMessage");
		String json=null;
		
		System.out.println("New Message onMessage event!");
		
		if (message instanceof TextMessage) {
			try{
				json=((TextMessage) message).getText();
				consumerAdapter.sendToMongo(json);
			}catch(JMSException e){
				logger.error("Message "+json+", a JMSException occurred: "+e.getErrorCode()+" "+e.getMessage());
				e.printStackTrace();
				jmsTemplate.convertAndSend(json);
				
			}catch(Throwable th){
				logger.error(th.getClass().getName()+ " : "+th.getMessage());
				th.printStackTrace();
				jmsTemplate.convertAndSend(json);
			}
		}
		
	}

}
